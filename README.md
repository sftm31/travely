# travely - Node.js Mashup-App

Author: **Samir Faycal Tahar M'Sallem**

Webbasierte Systeme - Klausurersatzleistung


Travely is a unique city data lookup solution to **gather data** about a city based on the country its located in.
Once you enter the app, you will have to write a city name and travely will create **added value** for you.
For instance: You enter a city name (e.g. Berlin) and travely gives you information about where Berlin is, which language is speaken in Berlin, Germany, and you will learn whether Berlin (=Germany) is a covid19 risk area. And you will see a map of Berlin for sure!

## Why travely?

Unlike other geo data lookup solutions, travely offers **thousands of cities** to lookup and that for free.
Travely also provides a **responsive** website design and its very **lightweight**.

## How does travely work?

```mermaid
graph LR
X(User Input) --City--> A
A[GeoDB Cities API] --Country--> B[Restcountries API]
A -- Latitude, longitude--> C[Google Maps API]
A -- Country--> D[Covid19 API]
A -- City data, nearest cities--> Y
B -- Language--> E[AppStore, PlayStore Links]
B -- Flag, country data--> Y(User Output)
C -- Map--> Y
D -- Cases, estimation--> Y
E --> Y

```

## Where does the data come from?

Travely accesses **4 essential restful APIs**  and forges from that a worthwhile output to the user:

- [GeoDB Cities API](https://rapidapi.com/wirefreethought/api/geodb-cities/details) - access over 267,000 thousand cities and receive country and city data
- [Restcountries API](https://restcountries.eu/) - get relevant data about countries, like language, population, capitals and flag data
-  [Covid19 API](https://documenter.getpostman.com/view/10808728/SzS8rjbc?version=latest) - retrieve current covid19 cases of each country and live statistics
- [Google Maps API](https://developers.google.com/maps/documentation/javascript/overview?hl=de) -  display high quality maps with live data everywhere in the world

## Accessibility

Travely follows the internet standard **WCAG 2.1 AA** and offers a **responsive design**, **mobile first**!

# Get in touch

You can view the live demo of this project [on Heroku](https://serene-chamber-16105.herokuapp.com/).

> As it is for developement purposes only, the Google Map will show a "for development purposes only" tag

## Developing with travely

Travely is working on node.js and express. It uses the website framework Bootstrap.

In order to work with this project locally you need to have git and node.js installed.

You need to get a [Rapidapi Account with GeoDB Cities Subscription](https://rapidapi.com/wirefreethought/api/geodb-cities/pricing) and a [Maps for JavaScript API Key](https://developers.google.com/maps/gmp-get-started?hl=de#procedures) 

### Installation:

1. Clone the project 
	- `git clone git@git.thm.de:sftm31/travely.git`
2.  In your project directory run:
	- `npm install`
3. Adding API Keys
	- Rename `.env_sample` to `.env`
	- In `.env` insert your rapidapi KEY to where `YOUR_API_KEY_HERE`
	- If you use the free subscription of GeoDB Cities, i recommend you to create a second account and paste the second key where `YOUR_BACKUP_KEY_HERE`. This is because of the restriction of the api service provider (requests per second).
	- Otherwise, just paste your regular api key twice in your `.env` file
	- In `index.html` right before the `</body>` replace at the `"maps.googleapis.com"` link the parameter `"key=YOUR_KEY"`
	- If you plan to run the app on heroku, you need to add the Rapidapi key manually to heroku with: `heroku config:set API_KEY=KEY_HERE` and `heroku config:set API_KEY_BACKUP=KEY_HERE`
4.  Once every dependency is installed run
	- `node index.js`
5. The app is now availiable under 
	- `http://localhost:1337/`

### Changing the project: 

1. Change port to bind on localhost
	- `in index.js change PORT`
2.  Static files are located in
	- `/img, /js, /css`
3.  Heroku start instructions are in
	- `Procfile`


# Copyright

Travely uses [Bootstrap](https://getbootstrap.com/) and [Stylish Portfolio](https://startbootstrap.com/themes/stylish-portfolio/). Copyright 2013-2020 Start Bootstrap LLC. Code released under the MIT license.

The API services of which travely retrieves its data are used for non-commercial purposes.

This version only uses free offers of each API service provider.

In order to use this service you need to obtain an api key for each service by yourself.

You are allowed to use this repo to continue working on the project.

You are not allowed to sell/rename this project or claim copyright for it.





