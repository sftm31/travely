const data = require('dotenv').config();
const fetch = require("node-fetch");
const PORT = process.env.PORT || 1337;
var express = require('express');
var path = require('path');


var app = express();
app.use("/css", express.static(__dirname + "/css"));
app.use("/img", express.static(__dirname + "/img"));
app.use("/js", express.static(__dirname + "/js"));
app.use("/scss", express.static(__dirname + "/scss"));
app.use("/vendor", express.static(__dirname + "/vendor"));

app.get('/',function(req,res){
  res.sendFile(path.join(__dirname + '/index.html'));
});
app.listen(PORT);


//API GET REQUESTS

//GEO DB API GET REQUEST --> GET NAME
  app.get('/geodb', async (request, response) => {

  let cityname = request.query.namePrefix; //getting parameter for the url

  let api_response = await fetch(`https://wft-geo-db.p.rapidapi.com/v1/geo/cities?namePrefix=${cityname}`, {
    "method": "GET",
    "headers": {
      "x-rapidapi-host": "wft-geo-db.p.rapidapi.com",
      "x-rapidapi-key": process.env.API_KEY //loading api key from .env file
    }
    
  });
  let json = await api_response.json();
  response.json(json);
  });

//GEO DB API GET REQUEST --> GET CITYDATA (use of different endpoint)
app.get('/geodb2', async (request, response) => {

  let id = request.query.cityid; //getting parameter for the url

  let api_response = await fetch(`https://wft-geo-db.p.rapidapi.com/v1/geo/cities/${id}`, {
    "method": "GET",
    "headers": {
      "x-rapidapi-host": "wft-geo-db.p.rapidapi.com",
      "x-rapidapi-key": process.env.API_KEY //loading api key from .env file
    }
    
  });
  let json = await api_response.json();
  response.json(json);
  });

//GEO DB API GET REQUEST --> GET NEAREST CITIES (use of different endpoint)
  app.get('/geodb3', async (request, response) => {

    let id = request.query.cityid; //getting parameter for the url
  
    let api_response = await fetch(`https://wft-geo-db.p.rapidapi.com/v1/geo/cities/${id}/nearbyCities?radius=100`, {
      "method": "GET",
      "headers": {
        "x-rapidapi-host": "wft-geo-db.p.rapidapi.com",
        "x-rapidapi-key": process.env.API_KEY_BACKUP //loading backup api key from .env file
      }
      
    });
    let json = await api_response.json();
    response.json(json);
    });
  
//RESTCOUNTRIES API GET REQUEST
app.get('/restcountries', async (request, response) => {
  let country = request.query.country;
  let api_response = await fetch(`https://restcountries.eu/rest/v2/name/${country}`); //no additional head data required as api doesnt need authentication
  let json = await api_response.json();
  response.json(json);
});

//COVID19 API GET REQUEST
app.get('/covid19', async (request, response) => {
  let api_response = await fetch(`https://api.covid19api.com/summary`); //no parameter needed because the client-side will filter the results for the specific country
  let json = await api_response.json();
  response.json(json);
});

