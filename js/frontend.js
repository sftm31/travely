//VISUAL FUNCTIONS (ADDING ELEMENTS TO HTML)

//loading spinner
function loading(boolean){
    const loading = document.getElementById("loading");

    if(boolean == false){
        loading.style.display = "none"; //setting loading invisible
    }
    else {
        loading.style.display = "inline-block"; //setting loading visible
    }

}

//display errorcode if defined boolean is given
function errorcode(boolean){
    const error = document.getElementById("error");
    error.classList.remove('animate__animated', 'animate__flash');

    if(boolean == false){
        error.style.display = "none"; //setting error message 
    }
    else {
        error.style.display = "initial"; //setting error message visible
        error.classList.add('animate__animated', 'animate__flash');
    }

}

//CLASSIFY IF AREA IS COVID 19 RISK AREA
function covidclassify(activeCases, newCases, population){
    document.getElementById("covid_risk").style.display = "none";
    document.getElementById("covid_warning").style.display = "none";
    document.getElementById("covid_safe").style.display = "none";
    var mode = 3;

    if(activeCases > 1000 || (newCases > 100 && population < 10000000) || newCases > 500){
        document.getElementById("covid_risk").style.display = "initial";
        console.log('risk area');
        mode = 1;
    }
    if((newCases < 300 && population < 20000000 || activeCases < 1000) && mode != 1) {
        document.getElementById("covid_warning").style.display = "initial";
        console.log('warning area');
        mode = 2;
    }
    if(mode == 3) {
        document.getElementById("covid_safe").style.display = "initial";
        console.log('safe area');
    }

}

//displays warning message for multiple cities
function warningcode(boolean){

    if(boolean == false){
        document.getElementById("warning").style.display = "none"; //setting warning message invisible
        document.getElementById("choosecity").style.display = "none"; //setting city choose form invisible
    }
    else {
        document.getElementById("warning").style.display = "initial"; //setting warning message visible
        document.getElementById("choosecity").style.display = "initial"; //setting city choose form visible
    }

}

//change html view by making "action area" visible
function changeView(){
    document.getElementById("datafield").style.display = "initial";
  
        $('html, body').animate({
            scrollTop: $("#datafield").offset().top
        }, 1000);
  
}

async function visualize(){
;
    changeView(); //changing view to page bottom where action area is
    clearMessages();
    document.getElementById("details").style.display = "none";
    console.log(geodb);
    document.getElementById("countryflag").src = restcountry[2];
    document.getElementById("cityname").textContent = geodb[0]+", "+geodb[1];
    covidclassify(covid[2], covid[3], restcountry[3]);
    var date = covid[6].split("T");
    var cities ="";

    for(var i = 12; i<Object.keys(geodb).length; i++){
        cities += geodb[i]+", ";
    }

    document.getElementById("city_data").innerHTML = geodb[0]+" is located in "+geodb[1]+". <br />"+
    "<br />Information about <strong>"+geodb[0]+
    "</strong>: <br />Area type: <strong>"+geodb[6]+
    "</strong> <br />Region: <strong>"+geodb[7]+"</strong> (<strong>"+geodb[8]+"</strong>)"+
    "</strong> <br />Population: <strong>"+geodb[9]+
    "</strong> <br />Timezone: <strong>"+geodb[10]+
    "</strong> <br />Height above sea level: <strong>"+geodb[11]+
    "</strong> <br />Nearest cities: <strong>"+cities+
    "</strong>";

    document.getElementById("country_data").innerHTML ="Information about <strong>"+geodb[1]+
    "</strong>: <br />Capital: <strong>"+restcountry[0]+
    "</strong> <br />Region: <strong>"+restcountry[6]+
    "</strong> <br />Spoken language(s): <strong>"+restcountry[1]+
    "</strong> <br />Population: <strong>"+restcountry[3]+
    "</strong> <br />Currency: <strong>"+restcountry[4][0].name+" ("+restcountry[4][0].symbol+")"+
    "</strong> <br />Native name(s): <strong>"+restcountry[5]+
    "</strong>";

    document.getElementById("covid_data").innerHTML = "Total confirmed COVID19 cases in "+geodb[1]+": <strong>"+covid[0]+
    "</strong> <br />Total recovered: <strong>"+covid[1]+
    "</strong> <br />Active cases: <strong>"+covid[2]+
    "</strong> <br />New cases: <strong>"+covid[3]+
    "</strong> <br />New death cases: <strong>"+covid[4]+
    "</strong> <br />Newly recovered: <strong>"+covid[5]+
    "</strong> <br />Date: "+date[0];

    document.getElementById("learning_button").innerHTML = "Learn "+restcountry[1]; 
    document.getElementById("learning_badge").setAttribute("href", "https://play.google.com/store/search?q=+"+restcountry[1]+"+&c=apps&hl=en");
    document.getElementById("learning_badge2").setAttribute("href", "itms-apps://search.itunes.apple.com/WebObjects/MZSearch.woa/wa/search?term="+restcountry[1]);
    
}

function clearMessages(){
    errorcode(false); 
    warningcode(false);
    loading(false);
    document.getElementById('test').value = '';
}

function resetMessages(){
    document.getElementById("datafield").style.display = "none";
    document.getElementById("city_data").innerHTML = "";
    document.getElementById("covid_data").innerHTML = "";
    document.getElementById("country_data").innerHTML = "";
    
}