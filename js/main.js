/*
* COPYRIGHT (C) SAMIR FAYCAL TAHAR M'SALLEM
* TECHNISCHE HOCHSCHULE MITTELHESSEN
* 
*/


var geodb = []; // CITYNAME; COUNTRY; LAT; LONG; ID; WIKIDATAID; TYPE; REGION; REGIONCODE; POPULATION; TIMEZONE; ELEVATIONMETERS; CITIES NEAR CITY;;

var restcountry = []; //CAPITAL; LANGUAGE; FLAG-SVG; POPULATION; CURRENCIES[]; ALT-SPELLINGS[]; REGION; 

var covid = []; //TOTAL-CONFIRMED; TOTAL-RECOVERED; ACTIVE; COVID-CONFIRMED-NEW; COVID-DEATH-NEW; COVID-RECOVERED-NEW; COVID-DATA-TIME;


async function mashup(){
    //IT ALL STARTED JUST WITH THE CITYNAME HERE BEGINS THE MASHUP
    loading(true);
    maps(geodb[2],geodb[3]); //accessing Google Maps API by using latitude and longitude ascertained by GeoDB API
    restcountries(geodb[1]); //looking up Restcountries API with country name ascertained by GeoDB 
    learning(restcountry[1]); //producing links to learning apps with countries language ascertained by Restcountries API
    setTimeout(citydetails(geodb[4]), 10000); //looking up indepth city details of the city ascertained by GeoDB API (use of different endpoint)
    setTimeout(nearcities(geodb[4]), 6000); //looking up cities near the city ascertained by GeoDB API (use of different endpoint)
    covid19(geodb[1]); //looking up Covid19 API with country name ascertained by GeoDB API and Restcountries API
    setTimeout(visualize, 2000); //wait for 2 seconds to make sure the api data arrived completely
}

function reset(){
    geodb = [];
    restcountry = [];
    covid = [];
}





  