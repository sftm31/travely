//BACKEND FUNCTIONS (API REQUESTS)
//FUNCTIONS TO CALL FETCH FROM SERVER.JS

//GEO DB API https://rapidapi.com/wirefreethought/api/geodb-cities
function lookup(cityname){

    let city = cityname;
    if(document.getElementById("datafield").style.display == "initial"){
        resetMessages();
        reset();
    }

    fetch(`/geodb?namePrefix=${city}`).then(response => { //fetching country data to city name

        response.json().then(response => {
        response.json = response;
        var citylenght = Object.keys(response.data).length;

        //no city found
        if(citylenght == 0){
            console.log('no city found');
            errorcode(true);
            warningcode(false);
        } 
        // 1 city found
        if(citylenght == 1){
            console.log('found one city');

            geodb.push(response.data[0].name);
            geodb.push(response.data[0].country);
            geodb.push(response.data[0].latitude);
            geodb.push(response.data[0].longitude);
            geodb.push(response.data[0].id);
            geodb.push(response.data[0].wikiDataId);


            mashup();
        }
        //more than 1 city found
        if (citylenght > 1) {
            console.log('multiple cities found');
            errorcode(false);
            warningcode(true);

            select = document.getElementById('selectCity');
            
            for (var i=0; i<citylenght; ++i){
                var opt = document.createElement('option');
                opt.value = response.data[i].name + "#" +response.data[i].country+"#"+response.data[i].latitude+"#"+response.data[i].longitude+"#"+response.data[i].id+"#"+response.data[i].wikiDataId;
                opt.innerHTML = response.data[i].name + ", "+response.data[i].region+", "+response.data[i].country;
                select.appendChild(opt);
            }
        }
        });
    });
}

function citydetails(id){

    fetch(`/geodb2?cityid=${id}`).then(response => {
        response.json().then(response => {
        response.json = response;
        geodb.push(response.data.type);
        geodb.push(response.data.region);
        geodb.push(response.data.regionCode);
        geodb.push(response.data.population);
        geodb.push(response.data.timezone);
        geodb.push(response.data.elevationMeters);

        });
    });
}

function nearcities(id){

    fetch(`/geodb3?cityid=${id}`).then(response => {
        response.json().then(response => {
        response.json = response;
        console.log(response);
        var citylenght = Object.keys(response.data).length;

        for(var i=1; i<citylenght; i++){
            geodb.push(response.data[i].name);
        }

        });
    });
}

//GOOGLE MAPS API https://developers.google.com/maps/documentation/javascript/overview?hl=de
function maps(lat, long) {
    var pos = {lat: +lat, lng: +long };
    var map = new google.maps.Map(document.getElementById("map"), {
      center: pos,
      zoom: 12
    });
    var marker = new google.maps.Marker({position: pos, map: map});

  }


//RESTCOUNTRIES API https://restcountries.eu/
function restcountries(country){

    fetch(`/restcountries?country=${country}`).then(response => {
        response.json().then(response => {
        response.json = response;

        restcountry.push(response[0].capital);
        restcountry.push(response[0].languages[0].name);
        restcountry.push(response[0].flag);
        restcountry.push(response[0].population);
        restcountry.push(response[0].currencies);
        restcountry.push(response[0].altSpellings);
        restcountry.push(response[0].subregion);
        });
    });
}

//COVID19 API https://documenter.getpostman.com/view/10808728/SzS8rjbc?version=latest
function covid19(country){

    fetch(`/covid19`).then(response => {
        response.json().then(response => {
        response.json = response;
        var data = response.Countries;

        for (var i=0; i<Object.keys(data).length; i++) {
            if (_.isEqual(data[i].Country, country)){
                break;
            } 
        }

        if(i < Object.keys(data).length){
            covid.push(data[i].TotalConfirmed);
            covid.push(data[i].TotalRecovered);
            covid.push(data[i].TotalConfirmed-data[i].TotalRecovered);
            covid.push(data[i].NewConfirmed);
            covid.push(data[i].NewDeaths);
            covid.push(data[i].NewRecovered);
            covid.push(data[i].Date);
        }
        });
    });
    return true;
}

//APPSTORE AND PLAYSTORE LINKS FOR LANGUAGE APPS
function learning(language){


}