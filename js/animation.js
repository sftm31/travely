

window.addEventListener("load", function(){
  const logo = document.getElementById("logo_main");
  const img = document.getElementById("img");
  const slogan = document.getElementById("slogan_main");
  const input = document.getElementById("input");
    logo.classList.add('animate__animated', 'animate__backInDown');
    img.classList.add('animate__animated', 'animate__pulse');
    logo.addEventListener('animationend', () => {
      slogan.style.display = "initial";
      slogan.classList.add('animate__animated', 'animate__fadeInLeft');
      input.style.display = "inline-flex";


    });


});